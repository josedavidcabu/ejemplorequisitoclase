/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import java.util.Scanner;
import Modelo.Persona;

/**
 * Su tarea consiste en crear dos objetos de la clase Persona y probar todos los
 * requisitos
 *
 * @author madar
 */
public class PruebaPersona {

    public static void main(String[] args) {
        Persona array[] = new Persona[2];
        Scanner entrada = new Scanner(System.in);

        try {
            for (int i = 0; i != array.length; i++) {
                long cedula;
                String nombre;
                short dia,mes,agno;
                System.out.println("Digite la cedula de la persona numero " + (i + 1));
                cedula=(entrada.nextLong());
                System.out.println("digite el nombre de la persona numero " + (i + 1));
                nombre=entrada.next();
                System.out.println("digite el dia de nacimiento de la persona numero " + (i + 1));
                dia=(entrada.nextShort());
                System.out.println("digite el mes de nacimiento de la persona numero " + (i + 1));
                mes=(entrada.nextShort());
                System.out.println("digite el año de nacimiento de la persona numero " + (i + 1));
                agno=(entrada.nextShort());
                Persona x=new Persona(cedula,nombre,dia,mes,agno);
                array[i]=x;
                
            }
            System.out.println(array[0].getNombre());
            System.out.println(array[0].toString());
            
            int comparador = ((Comparable) array[0]).compareTo(array[1]);
            if (comparador == 0) {
                System.out.println("Las personas son iguales");
            } else {
                if (comparador > 0) {
                    System.out.println("La primera persona es mayor");
                } else {
                    System.out.println("La segunda persona es mayor");
                }
            }
        } catch (java.util.InputMismatchException e) {
             System.err.println("Se esperaban datos numericos");
        }
    }

}
