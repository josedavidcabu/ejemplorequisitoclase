/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Estudiante;
import java.util.Scanner;
/**
 *
 * @author madar
 */
public class PruebaEstudiante {
    
    public static void main(String[] args) {
       Scanner entrada = new Scanner(System.in);
       Estudiante array []=new Estudiante [2];
       try{
           for(int i = 0 ; i!=array.length;i++){
       System.out.println("Escriba el codigo del estudiante "+i);
       long codigo = entrada.nextLong();
       System.out.println("Escriba el nombre del estudiante"+i );
       String nombre = entrada.next();
       Estudiante x = new Estudiante (codigo,nombre);
       array[i]=x;
       }
        //Utilizar requisito 2:
        System.out.println("El nombre del objeto x es:"+array[0].getNombre());
        //Utilizar requisito 3:
        if(array[0].equals(array[1]))
            System.out.println("El objeto x es igual al objeto y");
        else
            System.out.println("El objeto x NO es igual al objeto y");
        //Utilizar requisito 4:
        int comparador=((Comparable)array[0]).compareTo(array[1]);
        String msg="Son iguales x e y";
        switch(comparador)
        {
            case 1:{
                msg="X es mayor que Y";
                break;
            }
            case -1:
                msg="X es menor que Y";
                break;
            }
        
        System.out.println(msg);
        
        //Requisito 5:
        System.out.println(array[0].toString());
        System.out.println(array[1].toString());
       }
       catch(java.util.InputMismatchException e){
       System.err.println("Se esperaban datos numericos");
       }
       
        
        }
       
        
    }

