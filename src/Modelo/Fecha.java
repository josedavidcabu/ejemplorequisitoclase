
/**
 * Clase Fecha
 *
 * @author (Jose David Castillo Buitrago (1152021) )
 * josedavidcabu@ufps.edu.co)
 *
 */
package Modelo;
class Fecha {

    private int dia;
    private int mes;
    private int anio;

    public static final int[] diasDelMes = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    public Fecha(int dia, int mes, int anio) {
        this.anio = anio;
        this.mes = mes;
        this.dia = dia;
    }

    public Fecha(int numero) {
        this.anio = numero / 10000;
        this.mes = (numero % 10000) / 100;
        this.dia = (numero % 10000) % 100;
    }

    /**
     * @return the dia
     */
    public int getDia() {
        return dia;
    }

    /**
     * @param dia the dia to set
     */
    public void setDia(int dia) {
        this.dia = dia;
    }

    /**
     * @return the mes
     */
    public int getMes() {
        return mes;
    }

    /**
     * @param mes the mes to set
     */
    public void setMes(int mes) {
        this.mes = mes;
    }

    /**
     * @return the anio
     */
    public int getAnio() {
        return anio;
    }

    /**
     * @param anio the anio to set
     */
    public void setAnio(int anio) {
        this.anio = anio;
    }

    /**
     * Regresa true si ésta fecha es mayor que otra
     */
    public boolean esMayor(Fecha otra) {
        return !this.esIgual(otra) && !this.esMenor(otra);
    }//fin esMayor

    /**
     * Regresa true si ésta fecha es igual que otra
     */
    public boolean esIgual(Fecha otra) {
        boolean igual = false;
        igual = this.anio == otra.anio && this.mes == otra.mes && this.dia == otra.dia;
        return igual;
    }

    /**
     * Regresa true si ésta fecha es menor que otra
     */
    public boolean esMenor(Fecha otra) {
        boolean menor = false;
        if (!this.esIgual(otra)) {
            if (this.anio == otra.anio) {
                if (this.mes == otra.mes) {
                    menor = this.dia>otra.dia;
                } else {
                    menor = this.mes < otra.mes;
                }
            } else {
                menor = this.anio < otra.anio;
            }
        }
        return menor;
    }

    public boolean esFechaValida() {
        boolean valida = false;
        valida = this.mes >= 1 && this.mes <= 12 && this.dia > 0 && this.dia <= this.diasDelMes[this.mes - 1];
        return valida;
    }

    @Override
    public String toString() {
        return "Fecha{" + "dia=" + dia + ", mes=" + mes + ", anio=" + anio + '}';
    }
    

}
