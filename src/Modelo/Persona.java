/**
 * Clase Fecha
 *
 * @author (Jose David Castillo Buitrago (1152021) )
 * josedavidcabu@ufps.edu.co)
 *
 */
package Modelo;

/**
 * Su tarea consite en crear los 5 requisitos tomando en cuenta que su factor de
 * comparación es la FECHA DE NACIMIENTO
 *
 */
public class Persona implements Comparable {

    private long cedula;
    private String nombre;
    private Fecha fecha;

    public Persona() {
    }

    public Persona(long cedula, String nombre, short diaNacimiento, short mesNacimiento, short agnoNacimiento) {

        this.cedula = cedula;
        this.nombre = nombre;
        fecha = new Fecha(diaNacimiento, mesNacimiento, agnoNacimiento);
        if (!fecha.esFechaValida()) {
            throw new RuntimeException("Porfavor Ingrese una fecha valida");
        }

    }

    public long getCedula() {
        return cedula;
    }

    public String getNombre() {
        return nombre;
    }


    public void setCedula(long cedula) {
        this.cedula = cedula;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Fecha getFecha() {
        return fecha;
    }

    public void setFecha(Fecha fecha) {
        this.fecha = fecha;
    }


    @Override
    public boolean equals(Object obj) {
        boolean eq = true;
        if (this == obj) {
            eq = true;
        }
        if (obj == null) {
            eq = false;
        }
        if (getClass() != obj.getClass()) {
            eq = false;
        }
        final Persona other = (Persona) obj;
        if (!(this.fecha.equals(other.fecha))) {
            eq = false;
        
        
        }
        return eq;
    }

    @Override
    public int compareTo(Object obj) {
        short rta = -1;
        if (this == obj) {
            rta = 0;
        }
        final Persona other = (Persona) obj;

        if (this.fecha.esIgual(other.fecha)) {
            rta = 0;
        }
        if (this.fecha.esMayor(other.fecha)) {
            rta = 1;
        }
        return rta;
    }

    @Override
    public String toString() {
        return "Persona{" + "cedula=" + cedula + ", nombre=" + nombre + ", fecha=" + this.fecha.toString() + '}';
    }
}
