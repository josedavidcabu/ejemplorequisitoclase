/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author madar
 */
public class Estudiante implements Comparable{
    
    private long codigo;
    private String nombre;
    
    //Requisito 1:

    public Estudiante() {
    }

    public Estudiante(long codigo, String nombre) {
        
        this.codigo = codigo;
        this.nombre = nombre;
    }

    //Requisito 2:
    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    

    @Override
    public boolean equals(Object obj) {
        if (this == obj) { // Compara si los dos objetos están apuntando a la misma dirección de memoria-> los objetos son iguales
            return true;
        }
        if (obj == null) { // SI obj( objeto 2)  no es null, si es null no se puede hacer la comparación
            return false;
        }
        if (getClass() != obj.getClass()) { // los objetos son de la misma clase
            return false;
        }
        // final= permite que no se altere los atributos del objeto, que sólo voy a comparar
        final Estudiante other = (Estudiante) obj; //es necesario para trabajar con la subclase
        //
        if (this.codigo != other.codigo) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(Object obj) {
        if (this == obj) { // Compara si los dos objetos están apuntando a la misma dirección de memoria-> los objetos son iguales
            return 0;
        }
//      Validar a través de EXCEPCIONES    
    //if (obj == null) { // SI obj( objeto 2)  no es null, si es null no se puede hacer la comparación
//            return false;
//        }
//        if (getClass() != obj.getClass()) { // los objetos son de la misma clase
//            return false;
//        }
        // final= permite que no se altere los atributos del objeto, que sólo voy a comparar
        final Estudiante other = (Estudiante) obj; //es necesario para trabajar con la subclase
        /*
            ESTE COMPARETO ES CANDIDO(ingenuos-A FUERZA BRUTA) , POR QUE DEBERÍA SER UNA RESTA 
        */
        
        if(this.codigo==other.codigo)
            return 0;
        if(this.codigo>other.codigo)
            return 1;
      return -1;
    }

    @Override
    public String toString() {
        return "Estudiante{" + "codigo=" + codigo + ", nombre=" + nombre + '}';
    }
    
    
    
    
    
}
